// Code:                                                            // Comments:
import java.util.Scanner;
public class calculator {
    public static void main(String[] args) {
        double num1;
        double num2;
        double answer;
        char op;                                                    // Adding variables.

        Scanner reader = new Scanner(System.in);                    // Scanning user input.
        System.out.println("input first number");
        num1 = reader.nextDouble();                                 // Input & read first number.
        System.out.println("input second number");
        num2 = reader.nextDouble();                                 // Input & read first number.
        System.out.println("enter an operator (+, -, *, /): ");
        op = reader.next().charAt(0);                               // Input & read operator.

        switch (op) {
            case '+':
                answer = num1 + num2;
                break;
            case '-':
                answer = num1 - num2;
                break;
            case '*':
                answer = num1 * num2;
                break;
            case '/':
                answer = num1 / num2;
                break;                                             // This all changing answer to operator you have selected.
            default: System.out.println("enter correct operator");
                return;                                            // Stop program if operator is incorrect.
        }
        System.out.println("result:");                             // Counting & printing result.
        System.out.println(num1 + " " + op + " " + num2 + " = " + answer);
        }
    }
